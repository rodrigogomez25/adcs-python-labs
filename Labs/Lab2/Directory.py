from MyPowerList import MyPowerList

class Directory:

    def __init__(self):
        self.power_list = MyPowerList()

    def create_record(self, name, address, phone, email):
        self.power_list.add_item(name + ":" + address + ":" + phone + ":" + email + ":")

    def save_records(self, file_path):
        self.power_list.write_to_file(file_path)

    def load_records(self, file_path):
        self.power_list.read_from_file(file_path)

    def search_record(self, search_by, search_input):
        index = int
        if search_by == "name":
            index = 0
        if search_by == "address":
            index = 1
        if search_by == "phone":
            index = 2
        if search_by == "email":
            index = 3
        for item in self.power_list.list:
            data = item.split(":")
            if search_input.upper() in data[index].upper():
                print("Name: " + data[0] + "\n" + "Address: " + data[1] + "\n" + "Phone: " + data[2] + "\n" + "Email: " + data[3])
