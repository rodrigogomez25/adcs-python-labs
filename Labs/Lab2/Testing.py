from MyPowerList import MyPowerList
from Directory import Directory


def main():
    dir = Directory()
    dir.create_record("Rodrigo Gomez", "Av. Cubilete 185", "3325064203", "rodrigo.gomez43@gmail.com")
    dir.save_records("testing_directory")
    dir.load_records("testing_directory")
    dir.search_record("email", "gmail")


if __name__ == '__main__':
    main()
