class MyPowerList:
    def __init__(self):
        self.list = []

    def add_item(self, item):
        self.list.append(item)
        print("Item added: " + self.list[len(self.list) - 1])

    def remove_item(self, index):
        self.list.remove(self.list[index - 1])
        print("Item removed!")

    def sort(self):
        for i in range(len(self.list) - 1, 0, -1):
            for index in range(i):
                if self.list[index] > self.list[index + 1]:
                    temp = self.list[index]
                    self.list[index] = self.list[index + 1]
                    self.list[index + 1] = temp
        print(self.list)

    def l_merge(self, merge_list):
        temp = []
        for item in merge_list:
            temp.append(item)
        for item in self.list:
            temp.append(item)
        self.list = temp
        print(self.list)

    def r_merge(self, merge_list):
        for item in merge_list:
            self.list.append(item)
        print(self.list)

    def read_from_file(self, file_name):
        file_to_read = open(file_name)
        data = file_to_read.readlines()
        for item in data:
            self.list.append(item.translate({ord('\n'): None}))
        print(self.list)

    def write_to_file(self, file_name):
        file_to_write = open(file_name, 'w')
        for item in self.list:
            file_to_write.write(item + "\n")
        file_to_write.close()
