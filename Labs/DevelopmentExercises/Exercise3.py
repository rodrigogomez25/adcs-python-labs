import sys


def duplicates_list(elements):
    new_list = []
    for i in elements:
        if i not in new_list:
            new_list.append(i)
    for i in new_list:
        sys.stdout.write(str(i)),
        sys.stdout.write(","),
    print()


def duplicates_set(elements):
    new_set = set(elements)
    print(new_set)


def main():
    elements = [1, 1, 2, 3, 4, 5, 1, 5, 7, 7, 8, 9, 0, 0, 0, 0]
    duplicates_list(elements)
    duplicates_set(elements)
    elements = ['a', 1, 'b', 0, 'b', 1, 'e', 'r', 't', 'p', 'q']
    duplicates_list(elements)
    duplicates_set(elements)
    elements = []
    duplicates_list(elements)
    duplicates_set(elements)
    elements = [1, 1, 1, 1, 1, 12, 99, 3, 2, 3, 4, 5, 3, 2, 12, 7, 313, 4, 52, 352, 523, 52, 3, 4, 23, 2, 1, 23, 45, 6, 6, 7, 6, 9]
    duplicates_list(elements)
    duplicates_set(elements)


if __name__ == '__main__':
    main()