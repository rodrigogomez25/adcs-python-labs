def sample_mean(data):
    n = 0
    sample_mean = 0
    for i in data:
        sample_mean += i
        n += 1
    return sample_mean / n


def sample_standard_deviation(data):
    mew = 0
    n = 0
    for i in data:
        mew += i
        n += 1
    mew /= n
    adding = 0
    for i in data:
        adding = (i - mew) * (i - mew)
    adding /= n
    result = adding ** .5
    return result


def median(data):
    length = len(data)
    if length % 2 == 0:
        return (data[int(length/2)] + data[int(length/2) - 1]) / 2
    else:
        return data[int(length/2)]


def n_quartile(data, n):
    length = len(data)
    return (length + 1) * (n / 4)


def n_percent(number, n):
    return number * (n/100)


def main():
    data = [1, 2, 3, 4, 5]
    print(sample_mean(data))
    print(n_quartile(data,4))
    print(n_percent(100, 20))


if __name__ == '__main__':
    main()