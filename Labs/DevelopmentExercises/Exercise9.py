def decimal_to_roman(num):
    roman = ""
    temp = num - num % 1000
    for i in range(0, int(temp/1000)):
        roman += "M"
    num = num - temp
    temp = num - num % 900
    for i in range(0, int(temp/900)):
        roman += "CM"
    num = num - temp
    temp = num - num % 500
    for i in range(0, int(temp/500)):
        roman += "D"
    num = num - temp
    temp = num - num % 400
    for i in range(0, int(temp/400)):
        roman += "CD"
    num = num - temp
    temp = num - num % 100
    for i in range(0, int(temp/100)):
        roman += "C"
    num = num - temp
    temp = num - num % 90
    for i in range(0, int(temp/90)):
        roman += "XC"
    num = num - temp
    temp = num - num % 50
    for i in range(0, int(temp/50)):
        roman += "L"
    num = num - temp
    temp = num - num % 40
    for i in range(0, int(temp/40)):
        roman += "XL"
    num = num - temp
    temp = num - num % 10
    for i in range(0, int(temp/10)):
        roman += "X"
    num = num - temp
    temp = num - num % 9
    for i in range(0, int(temp/9)):
        roman += "IX"
    num = num - temp
    temp = num - num % 5
    for i in range(0, int(temp/5)):
        roman += "V"
    num = num - temp
    temp = num - num % 4
    for i in range(0, int(temp/4)):
        roman += "IV"
    num = num - temp
    temp = num - num % 1
    for i in range(0, int(temp/1)):
        roman += "I"
    num = num - temp
    print(roman)


def main():
    decimal_to_roman(3999)


if __name__ == '__main__':
    main()