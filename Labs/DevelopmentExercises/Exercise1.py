def main():
    print("Please give me a number")
    n = int(input())
    if n % 2 == 0:
        if n % 4 == 0:
            print("Please give me a number to divide by")
            check = int(input())
            if n % check == 0:
                print("numbers divide evenly")
            else:
                print("numbers do not divide evenly")
        else:
            print("Number is even")
    else:
        print("Number is odd")


if __name__ == '__main__':
    main()