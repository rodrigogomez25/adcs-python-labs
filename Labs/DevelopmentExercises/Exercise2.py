from random import seed
from random import randint


def main():
    guess = ""
    seed(1)
    while guess != "exit":
        print("Guess a number between 1 and 9")
        value = randint(1, 9)
        guess = input()
        try:
            n_guess = int(guess)
        except ValueError:
            n_guess = -1
        if n_guess != -1:
            if int(n_guess) > value:
                print("guessed too high ")
            else:
                if int(n_guess) < value:
                    print("guessed too low")
                else:
                    print("YOU GUESSED RIGHT!")


if __name__ == '__main__':
    main()