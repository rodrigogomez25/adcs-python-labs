def fibonacci(length):
    fibonacci = []
    for i in range(0, length):
        if i == 0:
            fibonacci.append(1)
        else:
            if i == 1:
                fibonacci.append(1)
            else:
                fibonacci.append(fibonacci[i-2] + fibonacci[i-1])
    print(fibonacci)


def main():
    #print("Define length of fibonacci series")
    #length = int(input())
    fibonacci(0)
    #fibonacci(0.5)
    fibonacci(1)
    fibonacci(3)
    fibonacci(8)
    fibonacci(2000)
    fibonacci(450000)
    fibonacci(-1)
    fibonacci('p')
    fibonacci([])

    #0,0.5,1,3,8,2000,450000,-1,p,[]


if __name__ == '__main__':
    main()
