from MyPowerList import MyPowerList


class Directory:

    def __init__(self):
        self.power_list = MyPowerList()

    def create_record(self, name, email, age, country):
        self.power_list.add_item(name + ":" + email + ":" + age + ":" + country + ":")

    def delete_record(self, index):
        self.power_list.remove_item(index)

    def save_records(self, file_path):
        self.power_list.write_to_file(file_path)

    def load_records(self, file_path):
        self.power_list.read_from_file(file_path)

    def search_record(self, search_by, search_input):
        index = int
        if search_by == "name":
            index = 0
        if search_by == "email":
            index = 1
        if search_by == "age":
            index = 2
        if search_by == "country":
            index = 3
        for item in self.power_list.list:
            data = item.split(":")
            if search_input.upper() in data[index].upper():
                print("Name: " + data[0] + "\n" + "Email: " + data[1] + "\n" + "Age: " + data[2] + "\n" + "Country of Origin: " + data[3])
