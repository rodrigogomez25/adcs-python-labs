from MyPowerList import MyPowerList
from Directory import Directory


def main():
    dir = Directory()
    dir.create_record("Rodrigo Gomez", "rodrigo.gomez43@gmail.com", "29", "Mexico")
    dir.create_record("Elena Alvarez", "elena.cecilia95@gmail.com", "24", "Mexico")
    dir.create_record("Naren Suria", "naren.suria@gmail.com", "54", "India")
    dir.save_records("testing_directory")
    dir.load_records("testing_directory")
    dir.search_record("email", "gmail")
    dir.search_record("age", "29")
    dir.delete_record(2)


if __name__ == '__main__':
    main()
