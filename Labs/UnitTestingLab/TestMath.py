import unittest
import math
import filecmp


class TestMath(unittest.TestCase):

    def test_math_ceil_for_one_dot_6(self):
        self.assertEqual(math.ceil(1.6), 2)

    def test_math_ceil_big_number(self):
        self.assertEqual(math.ceil(12141242315235.4669624912649), 12141242315236)

    def test_math_ceil_error(self):
        self.assertRaises(TypeError, math.ceil, "12.6")

    def test_math_factorial_one(self):
        self.assertEqual(math.factorial(1), 1)

    def test_math_factorial_negative(self):
        self.assertRaises(ValueError, math.factorial, -3)

    def test_math_factorial_decimal(self):
        self.assertRaises(ValueError, math.factorial, 1.33333)

    def test_math_pow_two_to_three(self):
        self.assertEqual(math.pow(2, 3), 8)

    def test_math_pow_square_root_of_nine(self):
        self.assertEqual(math.pow(9, 1/2), 3)

    def test_math_pow_error(self):
        self.assertRaises(ValueError, math.pow, -1, 1/2)

    def test_filecmp_cmp_equal_files(self):
        self.assertTrue(filecmp.cmp("/Users/regomez/PycharmProjects/UnitTestingLab/venv/file1.txt", "/Users/regomez/PycharmProjects/UnitTestingLab/venv/file2.txt"))

    def test_filecmp_cmp_different_files(self):
        self.assertFalse(filecmp.cmp("/Users/regomez/PycharmProjects/UnitTestingLab/venv/file1.txt", "/Users/regomez/PycharmProjects/UnitTestingLab/venv/file3.txt"))

    def test_filecmp_cmp_file_not_found(self):
        self.assertRaises(FileNotFoundError, filecmp.cmp, "/Users/regomez/PycharmProjects/UnitTestingLab/venv/file1.txt", "/Users/regomez/PycharmProjects/UnitTestingLab/venv/file4.txt")


if __name__ == '__main__':
    unittest.main()
