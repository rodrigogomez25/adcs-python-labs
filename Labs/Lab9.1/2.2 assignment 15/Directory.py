from MyPowerList import MyPowerList
import logging
import sys


class Directory:

    def __init__(self):
        self.power_list = MyPowerList()
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                            datefmt='%m-%d %H:%M',
                            filename='directory-logs.log',
                            filemode='w')
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
        self.log = logging.getLogger("directory-logger")

    def create_record(self, name, address, phone, email):
        self.log.info("Create record name : %s, address : %s, phone : %s, email : %s", name, address, phone, email)
        try:
            self.power_list.add_item(name + ":" + address + ":" + phone + ":" + email + ":")
        except:
            self.log.error("Error in create_record: %s", sys.exc_info()[0])

    def save_records(self, file_path):
        self.log.info("Save records to file path: %s", file_path)
        try:
            self.power_list.write_to_file(file_path)
        except:
            self.log.error("Error in save_records: %s", sys.exc_info()[0])

    def load_records(self, file_path):
        self.log.info("Load records from file path: %s", file_path)
        try:
            self.power_list.read_from_file(file_path)
        except:
            self.log.error("Error in load_records: %s", sys.exc_info()[0])

    def search_record(self, search_by, search_input):
        self.log.info("Search record by: %s with input: %s", search_by, search_input)
        try:
            index = int
            if search_by == "name":
                index = 0
            if search_by == "address":
                index = 1
            if search_by == "phone":
                index = 2
            if search_by == "email":
                index = 3
            for item in self.power_list.list:
                data = item.split(":")
                if search_input.upper() in data[index].upper():
                    print("Name: " + data[0] + "\n" + "Address: " + data[1] + "\n" + "Phone: " + data[2] + "\n" + "Email: " + data[3])
        except:
            self.log.error("Error in search_record: %s", sys.exc_info()[0])
