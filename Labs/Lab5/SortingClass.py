import csv
import os
import math


def merge(p_list, l, m, r):
    """Method merge

    This method receives an array to be divided into two and then be re-merged into one sorted array again,
    and 3 indexes:
    l: where the left array will begin
    m: the middle point
    r: where right array will begin
    """
    nl = m - l + 1
    nr = r - m

    l_list = [0] * (int(nl))
    r_list = [0] * (int(nr))

    for i in range(0, nl):
        l_list[i] = p_list[l + i]
    for j in range(0, nr):
        r_list[j] = p_list[m + 1 + j]

    i = 0
    j = 0
    k = l

    while i < nl and j < nr:
        if l_list[i] <= r_list[j]:
            p_list[k] = l_list[i]
            i += 1
        else:
            p_list[k] = r_list[j]
            j += 1
        k += 1

    while i < nl:
        p_list[k] = l_list[i]
        i += 1
        k += 1

    while j < nr:
        p_list[k] = r_list[j]
        j += 1
        k += 1


class SortingClass:
    """class SortingClass.

    This class will contain the methods and functions to read numbers lists from .csv files, to sort them, and to write
    the output into also .csv files
    """
    def __init__(self):
        """SortingClass constructor.

        Initializes the header list to receive from the file and the data list.
        """
        self.header = []
        self.list = []

    def set_input_data(self, file_path_name):
        """Method set_input_data

        This method reads the data from the csv file and puts it on the local lists

        """
        self.header = []
        self.list = []
        with open(file_path_name) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            count = 0
            for row in csv_reader:
                if count == 0:
                    self.header.append(row)
                else:
                    self.list.append(row)
                count += 1
        return self.list

    def set_output_data(self, file_path_name):
        """Method set_output_data

        This method saves the data into the csv file and from on the local lists

        """
        if os.path.exists(file_path_name):
            os.remove(file_path_name)
        count = 0
        with open(file_path_name, mode='w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(self.header[0])
            for row in self.list:
                try:
                    csv_writer.writerow(row)
                except csv.Error:
                    l_row = [row]
                    csv_writer.writerow(l_row)
                count += 1
        return count

    def merge_sort(self, p_list, l, r):
        """Method merge_sort

        This recursive method part of the merge sort algorithm, takes a list and the left and right indexes to start
        merge-sorting the list

        """
        if l < r:
            m = math.ceil((l + (r - 1)) / 2)
            self.merge_sort(p_list, l, m)
            self.merge_sort(p_list, m + 1, r)
            merge(p_list, l, m, r)

    def execute_merge_sort(self):
        """Method execute_merge_sort

        This method calls the merge_sort algorithm on the local list of data

        """
        n_list = []
        n = len(self.list)
        for row in self.list:
            n_list.append(int(row[0]))
        self.merge_sort(n_list, 0, n - 1)
        self.list = n_list
        n_first, n_last = n_list[0], n_list[n-1]
        return n_first, n_last
