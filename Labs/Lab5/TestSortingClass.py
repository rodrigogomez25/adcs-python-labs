import unittest
import os
from SortingClass import SortingClass


class TestSortingClass(unittest.TestCase):
    """class TestSortingClass.

    This class will contain the methods for unit testing on SortingClass
    """

    def test_set_input_data_small_file(self):
        """Method test_set_input_data_small_file

        This method runs a test for reading a small csv file of 100k numbers and assert the number loaded into
        the list

        """
        sorting = SortingClass()
        l_list = sorting.set_input_data("100k_numbers.csv")
        self.assertEqual(100000, len(l_list))

    def test_set_input_data_big_file(self):
        """Method test_set_input_data_big_file

        This method runs a test for reading a big csv file of a million numbers and assert the number loaded into
        the list

        """
        sorting = SortingClass()
        l_list = sorting.set_input_data("1M_numbers.csv")
        self.assertEqual(1000000, len(l_list))

    def test_set_output_data_exists(self):
        """Method test_set_output_data_exists

        This method runs a test for reading a small csv file of 100k numbers and load an output csv file,
        asserting the file was correctly created

        """
        sorting = SortingClass()
        sorting.set_input_data("100k_numbers.csv")
        sorting.set_output_data("test_exists.csv")
        self.assertTrue(os.path.exists("test_exists.csv"))

    def test_set_output_data_small_file(self):
        """Method test_set_input_data_small_file

        This method runs a test for reading a small csv file of 100k numbers and load an output csv file,
        asserting the number of rows created on the output csv

        """
        sorting = SortingClass()
        sorting.set_input_data("100k_numbers.csv")
        o_count = sorting.set_output_data("output_small.csv")
        self.assertEqual(100000, o_count)

    def test_set_output_data_big_file(self):
        """Method test_set_output_data_big_file

        This method runs a test for reading a big csv file of a million numbers and load an output csv file,
        asserting the number of rows created on the output csv

        """
        sorting = SortingClass()
        sorting.set_input_data("1M_numbers.csv")
        o_count = sorting.set_output_data("output_big.csv")
        self.assertEqual(1000000, o_count)

    def test_execute_merge_sort_small(self):
        """Method test_execute_merge_sort_small

        This method runs a test for reading a small csv file of 100k numbers, merge-sort them and load an output csv
        file, asserting the first and last numbers are correct

        """
        sorting = SortingClass()
        sorting.set_input_data("100k_numbers.csv")
        first, last = sorting.execute_merge_sort()
        sorting.set_output_data("sort_output_small.csv")
        n_min = min(sorting.list)
        n_max = max(sorting.list)
        self.assertTrue(first == n_min and last == n_max)

    def test_execute_merge_sort_big(self):
        """Method test_execute_merge_sort_big

        This method runs a test for reading a big csv file of a million numbers, merge-sort them and load an output csv
        file, asserting the first and last numbers are correct

        """
        sorting = SortingClass()
        sorting.set_input_data("1M_numbers.csv")
        first, last = sorting.execute_merge_sort()
        sorting.set_output_data("sort_output_big.csv")
        n_min = min(sorting.list)
        n_max = max(sorting.list)
        self.assertTrue(first == n_min and last == n_max)


if __name__ == '__main__':
    unittest.main()
